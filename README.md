#################  TWIGIFY #################

Adds sparkle and unicorns to your site. 

###Instructions###

1.  Download and install Unicorns module in a Drupal 7 installation.
2.  Enable it. 
3.  Assign the new Unicorns to a region.
4.  This will add a button to the page. When pressed, it will add a random unicorn to the page using the cornify.com API. 
